#!/usr/bin/env python

import pathlib

from unidecode import unidecode
from ruamel.yaml import YAML

yaml = YAML()

source = pathlib.Path('/tmp/manifest')
manifest = pathlib.Path('manifest')
for path in source.glob('**/*.yml'):
    data = yaml.load(path.read_text())
    path = path.relative_to(source)

    if data.get('type') != 'dataset':
        dest = manifest / path
        dest.parent.mkdir(0o755, parents=True, exist_ok=True)
        with dest.open('w') as f:
            yaml.dump(data, f)
        continue

    dataset = data
    dataset['name'] = 'datasets/' + unidecode(dataset['name'])

    for rname, resource in dataset.get('resources').items():
        if 'objects' not in resource:
            continue

        objects = resource['objects']
        del resource['objects']
        for models in objects.values():
            for name, model in models.items():
                external = model.pop('external', '')
                model = {
                    'type': 'model',
                    'name': dataset['name'] + '/' + unidecode(name),
                    'external': {
                        'dataset': dataset['name'],
                        'resource': rname,
                        'name': external,
                    },
                    **model,
                }

                dest = manifest / f'{model["name"]}.yml'
                print(f'convert {path} -> {dest}')
                dest.parent.mkdir(0o755, parents=True, exist_ok=True)
                with dest.open('w') as f:
                    yaml.dump(model, f)

    dest = manifest / f'{dataset["name"]}.dataset.yml'
    print(f'convert {path} -> {dest}')
    dest.parent.mkdir(0o755, parents=True, exist_ok=True)
    with dest.open('w') as f:
        yaml.dump(dataset, f)
