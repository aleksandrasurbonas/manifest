.. default-role:: literal

.. _matavimo-vienetai:

Matavimo vienetai
#################

Matavimo vienetai naudojant `SI simbolius`__,  pateikiami :data:`property.ref`
stulpelyje.

.. __: https://en.wikipedia.org/wiki/International_System_of_Units